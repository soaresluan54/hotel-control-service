package br.com.cvc.hotel.control.api;

import br.com.cvc.hotel.control.api.rest.RequestController;
import br.com.cvc.hotel.control.request.QuotationRequest;
import br.com.cvc.hotel.control.response.dto.QuotationDTO;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.io.Files;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWireMock(port = 9197)
class RequestControllerTest {

    private RequestController requestController;
    private static final String RESOURCE_DIRECTORY = "src/test/resources/__files/";
    private static  final Long HOTEL_ID = 1032L;
    private List<QuotationDTO> result;

    @Autowired
    public RequestControllerTest(RequestController requestController) {
        this.requestController = requestController;
    }

    @Test
    public void shoudGetHotelByCityCode(){
        mockHotels();
        result = requestController.find(HOTEL_ID);
        assertEquals(result.get(0).getCityName(),"Porto Seguro");
    }

    @Test
    public void shoulProcessQuotation() throws IOException {
        mockQuotation();
        mockHotels();
        result = requestController.find(buildQuotationRequest());
        assertEquals(result.get(0).getCityName(),"Porto Seguro");
    }

    protected void mockHotels(){

        stubFor(get(WireMock.urlEqualTo("/hotels/avail/1032"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBodyFile("find_hotels_1032.json")));

        requestController.find(1032L);
        verify(getRequestedFor(urlEqualTo("/hotels/avail/1032")));
    }

    protected void mockQuotation() throws IOException {
        WireMock.stubFor(
            WireMock.get(WireMock.urlEqualTo("/rs/quotation"))
                .withRequestBody(equalToJson(resource("quotation-file.json")))
                .willReturn(aResponse()
                .withStatus(200)
                .withBodyFile("find_hotels_1032.json")));
    }

    protected String resource(String payloadPath) throws IOException {
        File file = new File(RESOURCE_DIRECTORY + payloadPath);

        if (!file.exists()) {
            throw new IllegalArgumentException("Payload file not found: "
                    + RESOURCE_DIRECTORY + payloadPath);
        }

        return Files.toString(file, Charset.defaultCharset());
    }

    private QuotationRequest buildQuotationRequest() {
        return new QuotationRequest("20/01/2020", "25/01/2020",1032L, 2, 1);
    }
}