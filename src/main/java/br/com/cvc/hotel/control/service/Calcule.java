package br.com.cvc.hotel.control.service;

import br.com.cvc.hotel.control.resource.Romm;

import java.util.List;

public interface Calcule {

    List<Romm> ofRommns(List<Romm> romms, int amountDays);

    List<Romm> ofRommns(List<Romm> romms);
}
