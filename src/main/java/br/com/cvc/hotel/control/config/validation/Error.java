package br.com.cvc.hotel.control.config.validation;

public class Error {

    private String field;
    private String error;

    public Error(String field, String error) {
        this.field = field;
        this.error = error;
    }

    public String getField() {
        return field;
    }

    public String getError() {
        return error;
    }
}
