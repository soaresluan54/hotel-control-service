package br.com.cvc.hotel.control.response.dto;

import br.com.cvc.hotel.control.resource.Hotel;
import br.com.cvc.hotel.control.resource.Romm;

import java.util.List;
import java.util.stream.Collectors;

public class QuotationDTO {
    private Long id;
    private String cityName;
    private List<Romm> romms;

    public QuotationDTO(Hotel hotel) {
        this.id = hotel.getId();
        this.cityName = hotel.getCityName();
        this.romms = hotel.getRomms();
    }

    public Long getId() {
        return id;
    }

    public String getCityName() {
        return cityName;
    }

    public List<Romm> getRomms() {
        return romms;
    }

    public static List<QuotationDTO> convert(List<Hotel> hotels) {
        return hotels.stream().map(QuotationDTO::new).collect(Collectors.toList());
    }

}
