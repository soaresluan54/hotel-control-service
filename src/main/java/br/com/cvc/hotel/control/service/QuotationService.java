package br.com.cvc.hotel.control.service;

import br.com.cvc.hotel.control.gateway.CvcBackEndHotelGateway;
import br.com.cvc.hotel.control.request.QuotationRequest;
import br.com.cvc.hotel.control.resource.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static br.com.cvc.hotel.control.util.FormattDate.parseDate;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@Service
public class QuotationService implements ProcessQuotation {

    private final CvcBackEndHotelGateway cvcBackEndHotelGateway;
    private final CalculateValues calculateValues;

    @Autowired
    public QuotationService(CvcBackEndHotelGateway cvcBackEndHotelGateway, CalculateValues calculateValues) {
        this.cvcBackEndHotelGateway = cvcBackEndHotelGateway;
        this.calculateValues = calculateValues;
    }

    @Override
    public List<Hotel> processQuotation(List<Hotel> hotels, int amountDays) {
        hotels.parallelStream().map(Hotel::getRomms).
            forEach(romms -> calculateValues.ofRommns(romms, amountDays));
        return hotels;
    }

    @Override
    public List<Hotel> processQuotation(List<Hotel> hotels) {
        hotels.stream().map(Hotel::getRomms).forEach(calculateValues::ofRommns);
        return hotels;
    }

    @Override
    public final List<Hotel> process(Long cityCode){
        return getHotels(cityCode);
    }

    @Override
    public final List<Hotel> process(QuotationRequest quotationRequest) {

        List<Hotel> hotels = getHotels(quotationRequest);
        int amountDays = getDaysAmount(quotationRequest);
        return processQuotation(hotels, amountDays);
    }

    @Override
    public final List<Hotel> getHotels(QuotationRequest quotationRequest) {
        return cvcBackEndHotelGateway.findHotels(quotationRequest.getCityCode());
    }

    @Override
    public final List<Hotel> getHotels(Long cityCode) {
        return processQuotation(cvcBackEndHotelGateway.findHotels(cityCode));
    }

    private int getDaysAmount(QuotationRequest quotationRequest) {

        HashMap<String, LocalDate> dates = parseDate(quotationRequest);
        LocalDate checkin = dates.get("checkin");
        LocalDate checkout = dates.get("checkout");

        return checkout.compareTo(checkin);
    }
}
