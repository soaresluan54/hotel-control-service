package br.com.cvc.hotel.control.util;

import br.com.cvc.hotel.control.request.QuotationRequest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class FormattDate {

     public static HashMap<String, LocalDate> parseDate(QuotationRequest quotationRequest) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        HashMap<String, LocalDate> dateStringHashMap = new HashMap<>();

        LocalDate checkin = LocalDate.parse(quotationRequest.getCheckin(), formatter);
        LocalDate checkout = LocalDate.parse(quotationRequest.getCheckout(), formatter);

        dateStringHashMap.put("checkin", checkin);
        dateStringHashMap.put("checkout", checkout);

        return dateStringHashMap;
    }
}
