package br.com.cvc.hotel.control;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelControlServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelControlServiceApplication.class, args);

	}

}
