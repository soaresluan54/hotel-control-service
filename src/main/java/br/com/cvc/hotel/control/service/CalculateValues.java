package br.com.cvc.hotel.control.service;

import br.com.cvc.hotel.control.resource.PriceDetail;
import br.com.cvc.hotel.control.resource.Romm;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component
public class CalculateValues implements Calcule {

    @Override
    public List<Romm> ofRommns(List<Romm> romms, int amountDays) {

        List<PriceDetail> priceDetails = romms.parallelStream().map(Romm::getPriceDetail).collect(Collectors.toList());
        List<PriceDetail> byAmountOfDays = calculePriceDetailsByAmountOfDays(priceDetails, amountDays);
        List<PriceDetail> priceDetailCommissions = addCommission(byAmountOfDays);

        priceDetailCommissions.parallelStream().<Consumer<? super Romm>>map(priceDetail -> romm ->
                romm.setTotalPrice(calculeTotalPrice(priceDetail))).forEach(romms::forEach);
        return romms;
    }

    @Override
    public List<Romm> ofRommns(List<Romm> romms) {

        List<PriceDetail> priceDetails = romms.parallelStream().map(Romm::getPriceDetail).collect(Collectors.toList());
        List<PriceDetail> priceDetailCommissions = addCommission(priceDetails);

        priceDetailCommissions.parallelStream().<Consumer<? super Romm>>map(priceDetail -> romm ->
        romm.setTotalPrice(calculeTotalPrice(priceDetail))).forEach(romms::forEach);
        return romms;
    }

    private List<PriceDetail> calculePriceDetailsByAmountOfDays(List<PriceDetail> priceDetails, int amountDays) {

        priceDetails.forEach(priceDetail -> {
            double priceAjustedAdult = priceDetail.getPricePerDayAdult() * amountDays;
            double priceAjustedChild = priceDetail.getPricePerDayChild() * amountDays;
            priceDetail.setPricePerDayAdult(formatValue(priceAjustedAdult));
            priceDetail.setPricePerDayChild(formatValue(priceAjustedChild));
        });

        return priceDetails;
    }

    private List<PriceDetail> addCommission(List<PriceDetail> priceDetails) {

        priceDetails.forEach(priceDetail -> {
            double pricePerDayAdult = priceDetail.getPricePerDayAdult();
            double pricePerDayChild = priceDetail.getPricePerDayChild();
            double priceAdultCommission = pricePerDayAdult + (pricePerDayAdult / 0.7);
            double priceChildCommmission = pricePerDayChild + (pricePerDayChild / 0.7);
            priceDetail.setPricePerDayAdult(formatValue(priceAdultCommission));
            priceDetail.setPricePerDayChild(formatValue(priceChildCommmission));
        });

        return priceDetails;

    }

    private double calculeTotalPrice(PriceDetail priceDetailCommission) {

        double totalPrice = priceDetailCommission.getPricePerDayChild() +
                priceDetailCommission.getPricePerDayAdult();

        return formatValue(totalPrice);
    }

    private double formatValue(double price) {
        DecimalFormat formato = new DecimalFormat("#.##");
        return Double.parseDouble(formato.format(price));
    }

}
