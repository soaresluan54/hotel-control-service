package br.com.cvc.hotel.control.gateway;

import br.com.cvc.hotel.control.resource.Hotel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class CvcBackEndHotelGateway {

   private final Logger logger = LoggerFactory.getLogger(CvcBackEndHotelGateway.class);

   private RestTemplate restTemplate;
   private String cvcBackEndHotelURL;
   private final String URI = "/hotels/avail/%s";
   @Autowired
   public CvcBackEndHotelGateway(@Qualifier("cvcRestTemplate") RestTemplate template,
                                 @Value("${cvc-back-end-hotel.url}") String cvcBackEndHotelURL) {
      this.restTemplate = template;
      this.cvcBackEndHotelURL = cvcBackEndHotelURL;
   }

   @Cacheable(value = "${cityCode}")
   public List<Hotel> findHotels(@RequestBody Long cityCode) {

      ResponseEntity<List<Hotel>> response;
         response = restTemplate.exchange(String.format(cvcBackEndHotelURL + URI, cityCode),
                 HttpMethod.GET,
                 null,
                 new ParameterizedTypeReference<List<Hotel>>() {
                 });

      logger.info(response.getStatusCode().toString());

      return response.getBody();
   }
}
