package br.com.cvc.hotel.control.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Hotel {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("cityCode")
    private Long cityCode;

    @JsonProperty("cityName")
    private String cityName;

    @JsonProperty("rooms")
    private List<Romm> romms;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getCityCode() {
        return cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public List<Romm> getRomms() {
        return romms;
    }

    @Override
    public String toString() {
        return "HotelResource{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cityCode=" + cityCode +
                ", cityName='" + cityName + '\'' +
                ", romms=" + romms +
                '}';
    }
}
