package br.com.cvc.hotel.control.api.rest;

import br.com.cvc.hotel.control.request.QuotationRequest;
import br.com.cvc.hotel.control.resource.Hotel;
import br.com.cvc.hotel.control.service.QuotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import br.com.cvc.hotel.control.response.dto.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/rs")
public class RequestController {

    private final QuotationService quotationService;

    @Autowired
    public RequestController(QuotationService quotationService) {
        this.quotationService = quotationService;
    }

    @GetMapping(path = "/hotels/{cityCode}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<QuotationDTO> find(@PathVariable @Valid Long cityCode){
        List<Hotel> hotels = quotationService.process(cityCode);

        return QuotationDTO.convert(hotels);
    }

    @GetMapping(path = "/quotation",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<QuotationDTO> find(@RequestBody @Valid QuotationRequest quotationRequest){

        List<Hotel> hotels = quotationService.process(quotationRequest);

        return QuotationDTO.convert(hotels);
    }
}
