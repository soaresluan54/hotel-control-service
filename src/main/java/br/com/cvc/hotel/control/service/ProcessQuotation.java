package br.com.cvc.hotel.control.service;

import br.com.cvc.hotel.control.request.QuotationRequest;
import br.com.cvc.hotel.control.resource.Hotel;

import java.util.List;

public interface ProcessQuotation {

    List<Hotel> process(QuotationRequest quotationRequest);

    List<Hotel> process(Long cityCode);

    List<Hotel> getHotels(QuotationRequest quotationRequest);

    List<Hotel> getHotels(Long cityCode);

    List<Hotel> processQuotation(List<Hotel> hotels, int amountDays);

    List<Hotel> processQuotation(List<Hotel> hotels);


}
