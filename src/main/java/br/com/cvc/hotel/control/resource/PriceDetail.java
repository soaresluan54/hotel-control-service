package br.com.cvc.hotel.control.resource;

import com.fasterxml.jackson.annotation.JsonAlias;

public class PriceDetail {

    @JsonAlias("adult")
    private double pricePerDayAdult;

    @JsonAlias("child")
    private double pricePerDayChild;

    public double getPricePerDayAdult() {
        return pricePerDayAdult;
    }

    public double getPricePerDayChild() {
        return pricePerDayChild;
    }

    public void setPricePerDayAdult(double priceAjustedAdult) {
        this.pricePerDayAdult = priceAjustedAdult;
    }

    public void setPricePerDayChild(double priceAjustedChild) {
        this.pricePerDayChild = priceAjustedChild;
    }

    @Override
    public String toString() {
        return "PriceDetail{" +
                "pricePerDayAdult=" + pricePerDayAdult +
                ", pricePerDayChild=" + pricePerDayChild +
                '}';
    }
}
