package br.com.cvc.hotel.control.resource;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Romm {

    @JsonProperty("roomID")
    private Long id;

    @JsonProperty("categoryName")
    private String categoryName;

    @JsonProperty("priceDetail")
    @JsonAlias("price")
    private PriceDetail priceDetail;

    private double totalPrice;

    public Long getId() {
        return id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
    public PriceDetail getPriceDetail() {
        return priceDetail;
    }

    @Override
    public String toString() {
        return "Romm{" +
                "id=" + id +
                ", categoryName='" + categoryName + '\'' +
                ", priceDetail=" + priceDetail +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
