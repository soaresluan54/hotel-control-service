package br.com.cvc.hotel.control.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class QuotationRequest {

    @NotNull
    @JsonProperty("checkin")
    private String checkin;

    @NotNull
    @JsonProperty("checkout")
    private String checkout;

    @NotNull
    @JsonProperty("cityCode")
    private Long cityCode;

    @NotNull
    @JsonProperty("qtdAdult")
    private int qtdAdult;

    @NotNull
    @JsonProperty("qtdChild")
    private int qtdChild;

    public QuotationRequest(String checkin, String checkout,
                            Long cityCode, int qtdAdult, int qtdChild) {
        this.checkin = checkin;
        this.checkout = checkout;
        this.cityCode = cityCode;
        this.qtdAdult = qtdAdult;
        this.qtdChild = qtdChild;
    }

    public Long getCityCode() {
        return cityCode;
    }

    public String getCheckin() {
        return checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public int getQtdAdult() {
        return qtdAdult;
    }

    public int getQtdChild() {
        return qtdChild;
    }

    @Override
    public String toString() {
        return "Quotation{" +
                ", checkin=" + checkin +
                ", checkout=" + checkout +
                ", cityCode=" + cityCode +
                ", qtdAdult=" + qtdAdult +
                ", qtdChild=" + qtdChild +
                '}';
    }
}