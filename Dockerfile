FROM openjdk:8-jre-alpine

EXPOSE 8080
RUN apk --no-cache upgrade
RUN apk add --no-cache nss

ADD target/hotel-control-service-1.jar hotel-control-service-1.jar
ENTRYPOINT ["java", "-Xmx256m", "-Xss128m", "-Duser.timezone=America/Sao_Paulo","-jar","hotel-control-service-1.jar"]
