# Hotel-Control-Service
hotel-control-service is a microservice that searches hotels by region and makes commission calculations and reservation quotes.
### Getting Started
Project repository link 
https://bitbucket.org/soaresluan54/hotel-control-service/src/master/
```
git clone https://soaresluan54@bitbucket.org/soaresluan54/hotel-control-service.git
```
### build project
```
mvn clean install
```
### run project 
```
mvn spring-boot:run
```
### Prerequisites
You need java to run the application locally or docker to run the container, you can also install it in a kubernetes environment.
### Deployment locally
```
mvn clean install
mvn spring-boot:run
```
### Deployment Kubernetes
```
kubectl create namespace hotel-control-service
kubectl create -f /hotel-control-service-be-service.yaml
kubectl create -f /hotel-control-service-deployment.yaml
```
### Deployment docker
```
docker pull soaresluan54/hotel-control-service:0.0.6
docker run soaresluan54/hotel-control-service:0.0.6 
```
## Services
1) Search hotels by region making commission calculations
> **URL: https://localhost:8080/rs**
>
> **URL Kubernetes: http://hotel-control-service.hotel-control-service.<environment-kubernetes>/rs**

##### Resources
```
 /hotels/<idHotel>
```
```
 /quotation
```

##### Example response OK -  /hotels/<idHotel>
```json
{
    "id": 1,
    "cityName": "Porto Seguro",
    "romms": [
        {
            "totalPrice": 26971.11,
            "roomID": 0,
            "categoryName": "Standard",
            "priceDetail": {
                "pricePerDayAdult": 16666.56,
                "pricePerDayChild": 10304.55
            }
        }
    ]
}
```
##### Example response NOK - /hotels/<idHotel>
```json
[]
```
1) Search hotel passing parameters to quote by calculating the total value of the trip
#### Resource
```
 /quotation
```
##### body exemple -  /quotation
```json
{
	"cityCode" : "1032",
	"checkin"  : "20/01/2020",
	"checkout" : "25/01/2020",
	"qtdAdult" : "2",
	"qtdChild" :"1"
}
```
##### Example response OK -  /quotation
```json
{
    "id": 1,
    "cityName": "Porto Seguro",
    "romms": [
        {
            "totalPrice": 16666.56,
            "roomID": 0,
            "categoryName": "Standard",
            "priceDetail": {
                "pricePerDayAdult": 16666.56,
                "pricePerDayChild": 10304.55
            }
        }
    ]
}
```
##### Example response NOK -  /quotation
```json
[]
```
### Built With
* [JAVA](https://docs.oracle.com/javase/8/docs/api/index.html) - Used Java 8
* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring Boot](https://spring.io/projects/spring-boot) - The web framework used
* [Apache Tomcat](http://tomcat.apache.org/) - The web Application 
### Versioning
We use [DockerHub](https://hub.docker.com/r/soaresluan54/hotel-control-service) for versioning. For the versions available, see the [tags on this repository](https://hub.docker.com/repository/docker/soaresluan54/hotel-control-service/general). 
## Author
* **Luan Soares** 
[linkedin](www.linkedin.com/in/soaresluan)